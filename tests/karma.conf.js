module.exports = function (config) {
	config.set({

		basePath: '../',

		files: [
			'assets/js/vendor.js',
			'assets/js/app.js',
			'assets/js/custom.js',

			'tests/unit/**/*.js'
		],

		autoWatch: true,

		frameworks: [ 'jasmine' ],

		browsers: [ 'Chrome' ],

		plugins: [
			'karma-chrome-launcher',
			'karma-jasmine'
		],

		junitReporter: {
			outputFile: 'test_out/unit.xml',
			suite: 'unit'
		}
	});
};