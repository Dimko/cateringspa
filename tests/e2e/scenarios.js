'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('wishBoard App', function () {
	it('Should redirect index.html to index.html#/wishlist', function () {
		browser.get('index.html');
		browser.getLocationAbsUrl().then(function (url) {
			expect(url).toEqual('/wishlist');
		});
	});
});