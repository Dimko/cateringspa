'use strict';

describe('wishBoard.routes', function () {
	beforeEach(module('wishBoard.routes'));

	it('Should map routes to controllers', function () {
		inject(function ($route) {

			expect($route.routes[ '/wishlist' ].controller).toEqual('WishListCtrl');
			expect($route.routes[ '/wishlist' ].templateUrl).toEqual('app/components/shaurma/shaurma.html');

			expect($route.routes[ '/about' ].templateUrl).toEqual('app/components/about/about.html');
			expect($route.routes[ '/about' ].controller).toEqual('AboutCtrl');

			expect($route.routes[ '/dashboard' ].templateUrl).toEqual('app/components/dashboard/dashboard.html');
			expect($route.routes[ '/dashboard' ].controller).toEqual('DashboardCtrl');

			expect($route.routes[ null ].redirectTo).toEqual('/wishlist')
		});
	});
});