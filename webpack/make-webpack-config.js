var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

function extractForProduction(loaders) {
	return ExtractTextPlugin.extract('style', loaders.substr(loaders.indexOf('!')));
}

module.exports = function(options) {
	options.lint = fs.existsSync(path.join(__dirname, '/../.eslintrc')) && options.lint !== false;

	var devServerUrl = options.production ? null : 'http://localhost:8080';
	var localIdentName = options.production ? '[hash:base64]' : '[path]-[local]-[hash:base64:5]';

	var cssLoaders = 'style!css!autoprefixer'; //'style!css?localIdentName=' + localIdentName + '!autoprefixer';
	var lessLoaders = cssLoaders + '!less';
	var sassLoaders = cssLoaders + '!sass';

	if (options.production) {
		cssLoaders = extractForProduction(cssLoaders);
		sassLoaders = extractForProduction(sassLoaders);
		lessLoaders = extractForProduction(lessLoaders);
	}

	return {
		context: path.join(__dirname, '/../app'),
		entry: {
			main: './app.module.es6'
			//,404: './404.js'
		},
		output: {
			path: path.join(__dirname, '/../bundle'),
			filename: '[name].js',
			publicPath: (devServerUrl ? devServerUrl : '') + '/bundle/'
		},
		module: {
			loaders: [
				{ test: /\.es6$/, exclude: /node_modules/, loader: 'babel' },
				{ test: /\.css$/, loader: cssLoaders },
				{ test: /\.less$/, loader: lessLoaders },
				{ test: /\.scss$/, loader:  sassLoaders },
				{ test: /\.(jpe?g|png|gif|svg)(|\?[^!]*?)$/i, loaders: [ 'url?limit=10000', 'img?minimize' ] },
				{ test: /\.(woff|woff2|eot|ttf)(|\?[^!]*?)$/, loader: 'url-loader' },
				{ test: /\.hbs$/i, loader: 'raw-loader' },
				{ test: /\.json$/, loader: 'json-loader' },
				{ test: /\.ya?ml/, loader: 'json-loader!yaml-loader' },
				{ test: /\.ts$/, loader: 'babel!ts-loader' }
			],
			preLoaders: [
				{ test: /\.es6$/, exclude: /node_modules/, loader: 'eslint-loader' }
			]
		},
		resolve: {
			root: [
				path.join(__dirname, '/../app'),
				path.join(__dirname, '/../node_modules')
			],
			fallback: __dirname
		},
		resolveLoader: {
			root: [
				path.join(__dirname, '/../node_modules'),
				path.join(__dirname, '/custom-loaders')
			],
			alias: {
				'autoprefixer': 'autoprefixer-loader?browsers[]=last 2 version,browsers[]=IE 9'
			}
		},
		plugins: ([].concat(
			[
				new webpack.optimize.CommonsChunkPlugin('common.js'),
				new webpack.ProvidePlugin({
					$: "jquery",
					jQuery: "jquery",
					"window.jQuery": "jquery",
					_: "lodash"//,
					//angular: "angular"
				}),
				new webpack.DefinePlugin({
					PROD: !!options.production
				}),
				new webpack.optimize.DedupePlugin()
			], !options.production ? [] : [ // for production
				new ExtractTextPlugin("[name].css"),
				new webpack.optimize.UglifyJsPlugin({minimize: true})
			]
		)),

		// Loader configs
		eslint: {
			configFile: './.eslintrc'
		},
		imagemin: {
			gifsicle: { interlaced: false },
			jpegtran: {
				progressive: true,
				arithmetic: false
			},
			optipng: { optimizationLevel: 5 },
			pngquant: {
				floyd: 0.5,
				speed: 2
			},
			svgo: {
				plugins: [
					{ removeTitle: true },
					{ convertPathData: false }
				]
			}
		}
	};
};
