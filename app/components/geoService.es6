/**
 * GeoService
 */
class GeoService {
	constructor($http, $q) {
		this.$http = $http;
		this.$q = $q;
	}

	getLocations(address) {
		const params = {
			address: address,
			sensor: false
		};

		return this.$http.get('http://maps.googleapis.com/maps/api/geocode/json', { params: params })
				   .then((response) => {
					   return this.$q.resolve(response.data.results);
				   });
	}
}

GeoService.$inject = ['$http', '$q'];

export default GeoService;
