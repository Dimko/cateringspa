import angular from 'angular';

import ShaurmaService from './shaurma.service.es6';
import LocationService from '../location/location.service.es6';
import ShaurmaListComponent from './list/shaurmaList.component.es6';
import ShaurmaDetailComponent from './detail/shaurmaDetail.component.es6';
import ScrollEndsDirective from './../../directives/scrollends.directive.es6';
import ShaurmaPageController from './shaurmaPage.controller.es6';

export default angular
	.module('catering.shaurma', [])
	.service('ShaurmaService', ShaurmaService)
	.service('LocationService', LocationService)
	.directive('scrollEnds', ScrollEndsDirective.directiveFactory)
	.component('shaurmaList', ShaurmaListComponent)
	.component('shaurmaDetail', ShaurmaDetailComponent)
	.controller('ShaurmaPageController', ShaurmaPageController)
	.name;
