import ShaurmaListController from './shaurmaList.controller.es6';

const ShaurmaListComponent = {
	templateUrl: require('file!./shaurmaList.html'),
	controller: ShaurmaListController,
	bindings: {
		locationId: '<'
	}
};

export default ShaurmaListComponent;
