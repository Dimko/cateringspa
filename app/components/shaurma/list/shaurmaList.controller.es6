import Controller from '../../controller.es6';

class ShaurmaListController extends Controller {
	constructor(ShaurmaService, StorageService, Notification) {
		super();

		this.ShaurmaService = ShaurmaService;
		this.StorageService = StorageService;
		this.Notification = Notification;

		this.ShaurmaService.setLocationId(this.locationId);

		this.shaurmaOptions = this.ShaurmaService.getOptions();

		this.shaurmas = this.ShaurmaService.getItems();

		this.uiOptions = this.StorageService.getOptions('ui');

		this.canLoad = true;
		this.isLoading = false;
		this.isDeleting = false;
	}

	load() {
		if (!this.canLoad || this.isLoading) {
			return;
		}

		this.isLoading = true;

		this.ShaurmaService.load()
			.then((res) => {
				this.canLoad = res.canLoad;

				//this.Notification.success('Load success');
			})
			.catch(() => {
				//this.Notification.error('Load error');
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	delete(shaurma) {
		this.isDeleting = true;

		this.ShaurmaService.delete(shaurma)
			.then(() => {
				//this.Notification.success('DELETE success');
			})
			.catch((error) => {
				//this.Notification.error('DELETE error: ' + error);
			})
			.finally(() => {
				this.isDeleting = false;
			});
	}

	setSize(size) {
		this.uiOptions.size = size;

		this.StorageService.saveOptions();
	}

	setSort(sort) {
		this.isLoading = true;

		this.ShaurmaService.setSort(sort)
			.then((res) => {
				this.canLoad = res.canLoad;

				//this.Notification.success('sort success');
			})
			.catch(() => {
				//this.Notification.error('sort error');
			})
			.finally(() => {
				this.isLoading = false;
			});
	}
}

ShaurmaListController.$inject = ['ShaurmaService', 'StorageService', 'Notification'];

export default ShaurmaListController;
