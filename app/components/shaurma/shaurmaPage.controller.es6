import Controller from '../controller.es6';
import Location from '../../entities/Location.es6';

class ShaurmaPageController extends Controller {
	constructor(locationId, LocationService, Notification) {
		super();

		this.locationId = locationId;
		this.LocationService = LocationService;
		this.Notification = Notification;

		this.isLoading = false;

		this.location = {};

		this.load(locationId);
	}

	load(locationId) {
		this.isLoading = true;

		this.LocationService.get(locationId)
			.then((location) => {
				const newLocation = new Location(location);

				this.location = newLocation;
				//this.Notification.success('load success');
			})
			.catch((error) => {
				//this.Notification.error('load error: ' + error);
			})
			.finally(() => {
				this.isLoading = false;
			});
	}
}

ShaurmaPageController.$inject = ['locationId', 'LocationService', 'Notification'];

export default ShaurmaPageController;
