import './shaurmaDetail.scss';
import ShaurmaDetailController from './shaurmaDetail.controller.es6';

const ShaurmaDetailComponent = {
	templateUrl: require('file!./shaurmaDetail.html'),
	controller: ShaurmaDetailController,
	bindings: {
		shaurma: '<',
		onDelete: '&'
	}
};

export default ShaurmaDetailComponent;
