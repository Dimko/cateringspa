import Controller from '../../controller.es6';

class ShaurmaDetailController extends Controller {
	constructor(ShaurmaService, AuthService) {
		super();

		this.shaurmaService = ShaurmaService;
		this.authService = AuthService;

		this.auth = this.authService.getData();

		this.shaurma = {};
	}
}

ShaurmaDetailController.$inject = ['ShaurmaService', 'AuthService'];

export default ShaurmaDetailController;
