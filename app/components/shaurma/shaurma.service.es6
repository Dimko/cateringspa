import _ from 'lodash';

import Shaurma from '../../entities/Shaurma.es6';

/**
 * Получение списка шаурмы локации
 */
class ShaurmaService {
	constructor(AppConfig, StorageService, $http, $q) {
		this.AppConfig = AppConfig;
		this.StorageService = StorageService;
		this.$http = $http;
		this.$q = $q;

		this.locationId = null;

		this.url = `${this.AppConfig.apiUrl}/locations`;

		this.page = 0;
		this.items = [];

		this.isLoading = false;
		this.canLoad = true;

		this.options = this.StorageService.getOptions('shaurma');
	}

	setLocationId(locationId) {
		if (locationId === this.locationId) {
			return this.$q.reject();
		}

		this.locationId = locationId;

		this.url = `${this.AppConfig.apiUrl}/locations/${locationId}/shaurmas`;

		return this.reset();
	}

	getItems() {
		return this.items;
	}

	getOptions() {
		return this.options;
	}

	setSort(sort) {
		if (sort === this.options.sort) {
			this.options.way = this.options.way === 'asc' ? 'desc' : 'asc';
		} else {
			this.options.sort = sort;
		}

		this.StorageService.saveOptions();

		return this.reset();
	}

	load() {
		if (!this.canLoad || this.isLoading) {
			return this.$q.reject();
		}

		this.isLoading = true;

		return this.$http.get(this.url, {
					   params: Object.assign(this.options, { page: this.page })
				   })
				   .then((response) => {
					   _.forEach(response.data, (shaurma) => {
						   const newShaurma = new Shaurma(shaurma);

						   this.items.push(newShaurma);
					   });

					   this.isLoading = false;

					   if (response.data.length === this.options.per_page) {
						   this.page++;
					   } else {
						   this.canLoad = false;
					   }

					   return this.$q.resolve({ canLoad: this.canLoad });
				   })
				   .catch((response) => {
					   console.error(response.statusText);

					   return this.$q.reject();
				   });
	}

	delete(item) {
		return this.$http.delete(`${this.url}/${item.id}`)
				   .then(() => {
					   _.remove(this.items, function(o) {
						   return o.id === item.id;
					   });

					   return true;
				   })
				   .catch((response) => {
					   console.error(response.statusText);

					   return false;
				   });
	}

	get(id) {
		return this.$http.get(`${this.url}/${id}`)
				   .then((response) => {
					   return this.$q.resolve(response.data);
				   });
	}

	reset() {
		this.page = 0;
		this.items.length = 0;
		this.canLoad = true;
		this.isLoading = false;

		return this.load();
	}
}

ShaurmaService.$inject = ['AppConfig', 'StorageService', '$http', '$q'];

export default ShaurmaService;
