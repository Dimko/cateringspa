import _ from 'lodash';

import Location from '../../entities/Location.es6';

/**
 * Получение списка локаций
 */
class LocationService {
	constructor(AppConfig, StorageService, $http, $q) {
		this.StorageService = StorageService;
		this.$http = $http;
		this.$q = $q;

		this.url = `${AppConfig.apiUrl}/locations`;

		this.page = 0;

		this.items = [];

		this.isLoading = false;
		this.canLoad = true;

		this.options = this.StorageService.getOptions('location');
	}

	getItems() {
		return this.items;
	}

	getOptions() {
		return this.options;
	}

	setSort(sort) {
		if (sort === this.options.sort) {
			this.options.way = this.options.way === 'asc' ? 'desc' : 'asc';
		} else {
			this.options.sort = sort;
		}

		this.StorageService.saveOptions();

		return this.reset();
	}

	setGeo(longitude, latitude) {
		this.options.longitude = longitude;
		this.options.latitude = latitude;

		this.StorageService.saveOptions();

		return this.reset();
	}

	load() {
		if (!this.canLoad || this.isLoading) {
			return this.$q.reject();
		}

		this.isLoading = true;

		return this.$http.get(this.url, {
					   params: Object.assign(this.options, { page: this.page })
				   })
				   .then((response) => {
					   _.forEach(response.data, (location) => {
						   const newLocation = new Location(location);

						   this.items.push(newLocation);
					   });

					   this.isLoading = false;

					   if (response.data.length === this.options.per_page) {
						   this.page++;
					   } else {
						   this.canLoad = false;
					   }

					   return this.$q.resolve({ canLoad: this.canLoad });
				   })
				   .catch((response) => {
					   console.error(response.statusText);

					   return this.$q.reject();
				   });
	}

	delete(item) {
		return this.$http.delete(`${this.url}/${item._id}`)
				   .then(() => {
					   _.remove(this.items, function(o) {
						   return o._id === item._id;
					   });

					   return true;
				   })
				   .catch((response) => {
					   console.error(response.statusText);

					   return false;
				   });
	}

	get(id) {
		return this.$http.get(`${this.url}/${id}`)
				   .then((response) => {
					   return this.$q.resolve(response.data);
				   });
	}

	reset() {
		this.page = 0;
		this.items.length = 0;
		this.canLoad = true;
		this.isLoading = false;

		return this.load();
	}

	resetAddress() {
		if (!this.options.longitude && !this.options.latitude) {
			return this.$q.reject();
		}

		delete this.options.longitude;
		delete this.options.latitude;

		this.StorageService.deleteOptions('address');

		return this.reset();
	}
}

LocationService.$inject = ['AppConfig', 'StorageService', '$http', '$q'];

export default LocationService;
