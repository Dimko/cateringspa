import Controller from '../../controller.es6';

class LocationListController extends Controller {
	constructor(LocationService, StorageService, GeoService, Notification) {
		super();

		this.LocationService = LocationService;
		this.StorageService = StorageService;
		this.GeoService = GeoService;
		this.Notification = Notification;

		this.locationOptions = this.LocationService.getOptions();

		this.locations = this.LocationService.getItems();

		this.uiOptions = this.StorageService.getOptions('ui');

		this.canLoad = true;
		this.isLoading = false;
		this.isDeleting = false;

		this.address = this.StorageService.getOptions('address');
		this.addresses = [];

		if (!this.locations.length) {
			this.load();
		}
	}

	load() {
		if (!this.canLoad || this.isLoading) {
			return;
		}

		this.isLoading = true;

		this.LocationService.load()
			.then((res) => {
				this.canLoad = res.canLoad;

				//this.Notification.success('Load success');
			})
			.catch(() => {
				//this.Notification.error('Load error');
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	delete(location) {
		this.isDeleting = true;

		this.LocationService.delete(location)
			.then(() => {
				//this.Notification.success('DELETE success');
			})
			.catch((error) => {
				//this.Notification.error('DELETE error: ' + error);
			})
			.finally(() => {
				this.isDeleting = false;
			});
	}

	resetAddress() {
		this.address = null;
		this.isLoading = true;

		this.LocationService.resetAddress()
			.then((res) => {
				this.canLoad = res.canLoad;

				//this.Notification.success('reset success');
			})
			.catch(() => {
				//this.Notification.error('reset error');
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	updateAddress() {
		this.isLoading = true;

		this.StorageService.saveOptions('address', this.address);

		this.LocationService.setGeo(this.address.geometry.location.lng, this.address.geometry.location.lat)
			.then((res) => {
				this.canLoad = res.canLoad;

				//this.Notification.success('address success');
			})
			.catch(() => {
				//this.Notification.error('address error');
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	refreshAddresses(address) {
		if (!(address && address.length > 3)) return;

		this.GeoService.getLocations(address)
			.then((addresses) => {
				this.addresses = addresses;
			})
			.catch((error) => {
				//this.Notification.error(error.message);
			});
	}

	setSize(size) {
		this.uiOptions.size = size;

		this.StorageService.saveOptions();
	}

	setSort(sort) {
		this.isLoading = true;

		this.LocationService.setSort(sort)
			.then((res) => {
				this.canLoad = res.canLoad;

				//this.Notification.success('sort success');
			})
			.catch(() => {
				//this.Notification.error('sort error');
			})
			.finally(() => {
				this.isLoading = false;
			});
	}
}

LocationListController.$inject = ['LocationService', 'StorageService', 'GeoService', 'Notification'];

export default LocationListController;
