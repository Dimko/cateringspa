import './locationList.scss';
import LocationListController from './locationList.controller.es6';

const LocationListComponent = {
	templateUrl: require('file!./locationList.html'),
	controller: LocationListController
};

export default LocationListComponent;
