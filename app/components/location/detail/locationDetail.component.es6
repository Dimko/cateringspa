import './locationDetail.scss';
import LocationDetailController from './locationDetail.controller.es6';

const LocationDetailComponent = {
	templateUrl: require('file!./locationDetail.html'),
	controller: LocationDetailController,
	bindings: {
		location: '<',
		onDelete: '&'
	}
};

export default LocationDetailComponent;
