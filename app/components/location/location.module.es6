import '../../../node_modules/ui-select/dist/select.min.css';

import 'angular-sanitize';
import 'ui-select';

import angular from 'angular';

import LocationService from './location.service.es6';
import LocationListComponent from './list/locationList.component.es6';
import LocationDetailComponent from './detail/locationDetail.component.es6';
import ScrollEndsDirective from './../../directives/scrollends.directive.es6';

export default angular
	.module('catering.location', [
		'ngSanitize',
		'ui.select'
	])
	.service('LocationService', LocationService)
	.directive('scrollEnds', ScrollEndsDirective.directiveFactory)
	.component('locationList', LocationListComponent)
	.component('locationDetail', LocationDetailComponent)
	.name;
