class AboutController {
	constructor() {
		this.version = '0.1.0 ES2015';
	}
}

AboutController.$inject = [];

export default AboutController;
