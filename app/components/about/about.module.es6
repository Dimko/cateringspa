import angular from 'angular';

import AboutController from './about.controller.es6';

export default angular
	.module('catering.about', [])
	.controller('AboutController', AboutController)
	.name;
