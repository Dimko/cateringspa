/**
 * Базовый сервис
 */
class Service {
	constructor(AppConfig, $http) {
		this.$http = $http;

		this.url = AppConfig.apiUrl;
		this.resource = '';

		this.geo = null;
	}

	/**
	 * Получить постранично
	 * @param page {number}
	 * @param perPage {number}
	 * @returns {*}
	 */
	getPage(page = 0, perPage = 10) {
		let url = `${this.url}/${this.resource}/?page=${page}&per_page=${perPage}`;

		if (this.sort) {
			url += `&sort=${this.sort}`;
		}

		if (this.way) {
			url += `&way=${this.way}`;
		}

		if (this.geo) {
			url += `&nearme[longitude]=${this.geo.coords[0]}&nearme[latitude]=${this.geo.coords[1]}`;

			if (this.geo.distance) {
				url += `&distance=${this.geo.distance}`;
			}
		}

		return this.$http.get(url);
	}

	/**
	 * Получить запись
	 * @param id {number}
	 * @returns {*}
	 */
	get(id) {
		return this.$http.get(`${this.url}/${this.resource}/${id}`);
	}

	/**
	 * Удалить запись
	 * @param id {number}
	 * @returns {*}
	 */
	delete(id) {
		return this.$http.delete(`${this.url}/${this.resource}/${id}`);
	}

	setSort(sort = 'grade', way = 'desc') {
		this.sort = sort;
		this.way = way;
	}

	/**
	 * Установка геопривязки
	 * @param coords {Array}
	 * @param distance {number}
	 */
	setGeo(coords, distance) {
		if (!coords) {
			this.geo = null;

			return;
		}

		this.geo = {};

		this.geo.coords = coords;

		if (distance) {
			this.geo.distance = distance;
		} else {
			this.geo.distance = null;
		}
	}
}

Service.$inject = ['AppConfig', '$http'];

export default Service;
