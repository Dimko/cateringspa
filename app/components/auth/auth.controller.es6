import Controller from '../controller.es6';

class AuthController extends Controller {
	constructor(AuthService, ngDialog) {
		super();

		this.authService = AuthService;
		this.ngDialog = ngDialog;
		this.data = AuthService.getData();
	}

	loginDialog() {
		this.ngDialog.open({
			template: require('file!./auth.html'),
			className: 'ngdialog-theme-default',
			controller: AuthController,
			controllerAs: 'auth'
		});
	}

	login(username, password) {
		if (!this.data.user) {
			this.authService.login(username, password);
		}
	}

	logout() {
		if (this.data.user) {
			this.authService.logout();
		}
	}
}

AuthController.$inject = ['AuthService', 'ngDialog'];

export default AuthController;
