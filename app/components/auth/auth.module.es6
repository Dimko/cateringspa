import angular from 'angular';

import AuthService from './auth.service.es6';
import AuthController from './auth.controller.es6';

export default angular
	.module('catering.auth', [])
	.service('AuthService', AuthService)
	.controller('AuthController', AuthController)
	.name;
