import User from '../../entities/User.ts';

/**
 * Сервис авторизации
 */
class AuthService {
	constructor(AppConfig, StorageService, $http) {
		this.StorageService = StorageService;
		this.$http = $http;

		this.url = AppConfig.apiUrl + '/sessions';

		this.token = null;
		this.data = {};

		this.init();
	}

	init() {
		const user = this.StorageService.getOptions('user');
		const token = this.StorageService.getOptions('token');

		if (user) {
			this.data.user = user;
		}

		if (token) {
			this.token = token;

			this.$http.defaults.headers.common.Authorization = this.token;
		}
	}

	getData() {
		return this.data;
	}

	/**
	 * Login
	 * @returns {*}
	 */
	login(username, password) {
		this.$http
			.post(`${this.url}/login`, {
				username: username,
				password: password
			})
			.then((response) => {
				this.token = response.data.token;
				this.data.user = new User(response.data.user);

				this.StorageService.saveOptions('token', this.token);
				this.StorageService.saveOptions('user', this.data.user);

				this.$http.defaults.headers.common.Authorization = this.token;
			});
	}

	/**
	 * Login
	 * @returns {*}
	 */
	logout() {
		this.$http
			.post(`${this.url}/logout`)
			.finally(() => {
				this.token = null;
				this.data.user = null;

				this.StorageService.deleteOptions('token');
				this.StorageService.deleteOptions('user');
			});
	}
}

AuthService.$inject = ['AppConfig', 'StorageService', '$http'];

export default AuthService;
