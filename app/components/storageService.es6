/**
 * StorageService
 */
class StorageService {
	constructor() {
		this.options = null;

		this.load();

		if (!(this.options && this.options.ui && this.options.shaurma && this.options.location)) {
			this.init();
			this.save();
		}
	}

	init() {
		this.options = {
			ui: { size: 'list' },
			shaurma: {
				per_page: 20,
				sort: 'grade',
				way: 'desc'
			},
			location: {
				per_page: 20,
				sort: 'grade',
				way: 'desc'
			}
		};
	}

	load() {
		this.options = JSON.parse(localStorage.getItem('options'));
	}

	save() {
		localStorage.setItem('options', JSON.stringify(this.options));
	}

	saveOptions(type = null, options = {}) {
		if (type && options) {
			this.options[type] = options;
		}

		this.save();
	}

	deleteOptions(type = null) {
		if (type) {
			delete this.options[type];
		}

		this.save();
	}

	getOptions(type) {
		return this.options[type];
	}
}

StorageService.$inject = [];

export default StorageService;
