/**
 * Базовая директива
 */
class Directive {
	constructor() {
		this.restrict = 'A';
	}

	link(scope, element, attributes) {

	}

	static directiveFactory() {
		Directive.instance = new Directive();
		return Directive.instance;
	}
}

Directive.$inject = [];

export default Directive;