import angular from 'angular';

let $selfLocation = null;

/**
 * активный пункт в меню
 */
class AutoActiveDirective {
	constructor($location) {
		this.restrict = 'A';
		this.scope = false;
		$selfLocation = $location;
	}

	link(scope, element) {
		const setActive = () => {
			const path = $selfLocation.path();

			if (path) {
				angular.forEach(element.find('div'), function(div) {
					const anchor = div.querySelector('a');

					if (anchor.href.match('#' + path + '(?=\\?|$)')) {
						angular.element(div)
							   .addClass('menu__item_active');
					} else {
						angular.element(div)
							   .removeClass('menu__item_active');
					}
				});
			}
		};

		setActive();

		scope.$on('$locationChangeSuccess', setActive);
	}

	/**
	 * Фабрика директивы
	 * @returns {AutoActiveDirective}
	 */
	static directiveFactory($location) {
		AutoActiveDirective.instance = new AutoActiveDirective($location);
		return AutoActiveDirective.instance;
	}
}

AutoActiveDirective.directiveFactory.$inject = ['$location'];

export default AutoActiveDirective;
