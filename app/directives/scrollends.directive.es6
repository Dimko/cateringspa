/**
 * Скролл до конца
 * Атрибуты: scrollEnds, threshold, delay
 */
class ScrollEndsDirective {
	constructor() {
		this.restrict = 'A';
	}

	link(scope, element, attributes) {
		const visibleHeight = $(window)
			.height();
		const threshold = attributes.threshold || 100;
		const delay = attributes.delay || 100;

		$(window)
			.scroll(_.debounce(() => {
				const scrollableHeight = document.body.scrollHeight;
				const hiddenContentHeight = scrollableHeight - visibleHeight;

				if (hiddenContentHeight - $(window)
						.scrollTop() <= threshold) {
					scope.$apply(attributes.scrollEnds);
				}
			}, delay));
	}

	/**
	 * Фабрика директивы
	 * @returns {ScrollEndsDirective}
	 */
	static directiveFactory() {
		ScrollEndsDirective.instance = new ScrollEndsDirective();
		return ScrollEndsDirective.instance;
	}
}

ScrollEndsDirective.$inject = [];

export default ScrollEndsDirective;
