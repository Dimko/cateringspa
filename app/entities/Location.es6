import _ from 'lodash';

import Item from './Item.ts';
import Shaurma from './Shaurma.es6';

/**
 * Локация содержащая в себе шаурму
 */
class Location extends Item {
	constructor(location) {
		super();

		this._id = location._id;
		this.id = location.id;
		this.name = location.name;
		this.loc = location.loc;
		this.count = location.count;
		this.shaurmas = [];

        this.shaurmas = _.map(location.shaurmas, shaurma => new Shaurma(shaurma));

		this.grade = location.grade ? location.grade.toFixed(1) : '0.0';
		this.gradeFloat = location.grade || 0;
		this.color = this.calculateColor(this.gradeFloat);
	}
}

export default Location;
