/**
 * Отдельная единица
 */
class Item {
    constructor() {
    }

    /**
     * Определение цвета в зависимости от оценки
     * @param grade {string}
     * @returns {string}
     */
    calculateColor(grade:number):string {
        const color = this.pickColor([82, 200, 0], [200, 50, 50], grade / 10);

        return `rgb(${color})`;
    }
    
    /**
     * Получение цвета из градиета
     * @param color1 {[]}
     * @param color2 {[]}
     * @param weight {number}
     * @returns {*[]}
     */
    pickColor(color1:Array<number>, color2:Array<number>, weight:number):Array<number> {
        const w = weight * 2 - 1;
        const w1 = (w + 1) / 2;
        const w2 = 1 - w1;

        return [Math.round(color1[0] * w1 + color2[0] * w2),
            Math.round(color1[1] * w1 + color2[1] * w2),
            Math.round(color1[2] * w1 + color2[2] * w2)];
    }
}

export default Item;