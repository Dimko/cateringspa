import Item from './Item.ts';

/**
 * Шаурма
 */
class Shaurma extends Item {
	constructor(shaurma) {
		super();

		this.id = shaurma._id; // todo rename to _id
		this.link = shaurma.link;
		this.thumbnail = shaurma.thumbnail;
		this.low_resolution = shaurma.low_resolution;
		this.standard_resolution = shaurma.standard_resolution;
		this.username = shaurma.username;
		this.profile_picture = shaurma.profile_picture;
		this.grade = shaurma.grade ? shaurma.grade.toFixed(1) : '0.0';
		this.gradeFloat = shaurma.grade || 0;

		this.color = this.calculateColor(this.gradeFloat);
	}
}

export default Shaurma;
