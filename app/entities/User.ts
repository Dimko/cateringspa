/**
 * Пользователь
 */
class User {
	private id:string;
	private username:string;
	private profile_picture:string;
	private role:string;

	constructor(user) {
		this.id = user.id;
		this.username = user.username;
		this.profile_picture = user.profile_picture;
		this.role = user.role;
	}
}

export default User;
