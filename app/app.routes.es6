import './components/location/location.service.es6';

function config($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: require('file!./components/location/locationPage.html')
		})
		.when('/locations/:locationId', {
			templateUrl: require('file!./components/shaurma/shaurmaPage.html'),
			controller: 'ShaurmaPageController',
			controllerAs: 'ShaurmaPageCtrl',
			resolve: {
				locationId: ['$route', function($route) {
					return $route.current.params.locationId;
				}]
			}
		})
		.when('/about', {
			templateUrl: require('file!./components/about/about.html'),
			controller: 'AboutController',
			controllerAs: 'about'
		})
		.otherwise({
			redirectTo: '/'
		});
}

config.$inject = ['$routeProvider'];

export default config;
