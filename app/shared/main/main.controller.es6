class MainController {
	constructor($rootScope, AuthService) {
		this.name = 'Рейтинг шаурмы';

		this.auth = AuthService.getData();
	}
}

MainController.$inject = ['$rootScope', 'AuthService'];

export default MainController;
