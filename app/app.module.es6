import 'shared/main/main.scss';

import angular from 'angular';

import 'angular-route';
import 'ngstorage';
import 'angular-animate';
import 'ng-dialog';
import 'angular-ui-notification';

import '../node_modules/angular-ui-notification/dist/angular-ui-notification.min.css';

import routes from 'app.routes.es6';

import location from 'components/location/location.module.es6';
import shaurma from 'components/shaurma/shaurma.module.es6';
import about from 'components/about/about.module.es6';
import auth from 'components/auth/auth.module.es6';

import AutoActiveDirective from 'directives/autoActive.directive.es6';
import GeoService from 'components/geoService.es6';
import StorageService from 'components/storageService.es6';

import MainController from 'shared/main/main.controller.es6';
import NavBarController from 'shared/navBar/navBar.controller.es6';

export default angular
	.module('catering', [
		'ngStorage',
		'ngAnimate',
		'ngRoute',
		'ngDialog',
		'ui-notification',

		auth,
		about,
		location,
		shaurma
	])
	.config(['NotificationProvider', (NotificationProvider) => {
		NotificationProvider.setOptions({
			delay: 10000,
			startTop: 20,
			startRight: 10,
			verticalSpacing: 20,
			horizontalSpacing: 20,
			positionX: 'right',
			positionY: 'bottom'
		});
	}])
	.constant('AppConfig', {
		apiUrl: 'https://shaurma-dimkos.rhcloud.com'
		//apiUrl: 'http://localhost:3003'
	})
	.directive('autoActive', AutoActiveDirective.directiveFactory)
	.service('GeoService', GeoService)
	.service('StorageService', StorageService)
	.controller('MainController', MainController)
	.controller('NavBarController', NavBarController)
	.config(routes)
	.name;
